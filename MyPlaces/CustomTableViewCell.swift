//
//  CustomTableViewCell.swift
//  MyPlaces
//
//  Created by Maksim Isaev on 29.01.2020.
//  Copyright © 2020 Maksim Isaev. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var imageOfPlace: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
}
