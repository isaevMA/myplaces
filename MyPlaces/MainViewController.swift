//
//  MainViewController.swift
//  MyPlaces
//
//  Created by Maksim Isaev on 28.01.2020.
//  Copyright © 2020 Maksim Isaev. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController {
    
    let restaurantsNames = [
        "Пиццамания", "Помидор", "Персона",
        "4 сыра", "American Chicken", "Безумный цыпленок",
        "Гавар", "Oregano", "Dodo Pizza", "Drova"
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // возвращает количество строк
        return restaurantsNames.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell // прикрепили созданный класс для работы с ячейкой
        

        cell.nameLabel.text = restaurantsNames[indexPath.row]
        cell.imageOfPlace.image = UIImage(named: restaurantsNames[indexPath.row])
        cell.imageOfPlace.layer.cornerRadius = cell.imageOfPlace.frame.size.height / 2
        cell.imageOfPlace.clipsToBounds = true

        return cell
    }
    
    // MARK: - Table View Deligate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
